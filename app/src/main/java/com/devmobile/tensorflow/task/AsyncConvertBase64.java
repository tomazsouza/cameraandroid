package com.devmobile.tensorflow.task;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class AsyncConvertBase64 extends AsyncTask<Bitmap, Void, String> {

    private Base64Result mBase64Result;

    public AsyncConvertBase64(Base64Result base64Result) {
        this.mBase64Result = base64Result;
    }

    @Override
    protected String doInBackground(Bitmap... bitmaps) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        if (bitmaps[0] != null) {
            bitmaps[0].compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        }

        byte[] bytes = byteArrayOutputStream.toByteArray();
        String encodeImage = Base64.encodeToString(bytes, Base64.DEFAULT);
        return encodeImage;
    }

    @Override
    protected void onPostExecute(String s) {
        if (s == null) {
            this.mBase64Result.errorConvertBase64("Error convert file");
        } else {
            this.mBase64Result.resultConverterBase64(s);
        }
    }

    public interface Base64Result {
        void resultConverterBase64(@NonNull String imageBase64);

        void errorConvertBase64(@NonNull String error);
    }

}
