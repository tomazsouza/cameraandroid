package com.devmobile.tensorflow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

@SuppressLint("ViewConstructor")
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder mSurfaceHolder;
    private Camera mCamera;
    private String TAG = "CameraPrevire";

    public CameraPreview(@NonNull Context context, @NonNull Camera camera) {
        super(context);

        this.mCamera = camera;

        this.mSurfaceHolder = getHolder();
        this.mSurfaceHolder.addCallback(this);
//        this.mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (mCamera != null && mSurfaceHolder != null) {
                this.mCamera.setPreviewDisplay(mSurfaceHolder);
                this.mCamera.startPreview();
            }
        } catch (IOException e) {
            if (e != null) {
                e.printStackTrace();
                Log.d(TAG, "Error setting camera preview: " + e.getMessage());
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        if (mSurfaceHolder != null) {
            if (mSurfaceHolder.getSurface() == null) {
                return;
            }

            try {

                if (mCamera != null) {

                    this.mCamera.startPreview();

                }

            } catch (Exception e) {
                if (e != null) e.printStackTrace();
            }

            try {

                if (mCamera != null && mSurfaceHolder != null) {
                    this.mCamera.setPreviewDisplay(mSurfaceHolder);
                    this.mCamera.startPreview();
                }

            } catch (Exception e) {
                if (e != null) {
                    Log.d(TAG, "Error starting camera preview: " + e.getMessage());
                }
            }
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

}
