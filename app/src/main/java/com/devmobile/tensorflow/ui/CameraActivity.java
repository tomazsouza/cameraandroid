package com.devmobile.tensorflow.ui;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.devmobile.tensorflow.CameraPreview;
import com.devmobile.tensorflow.R;
import com.devmobile.tensorflow.capturepicture.CapturePicture;

import java.io.IOException;

public class CameraActivity extends AppCompatActivity implements
        View.OnClickListener,
        CapturePicture.ListenerPicture {

    private Camera mCamera;
    private CameraPreview mCameraPreview;
    private AppCompatButton mBtnCapture;
    private Bitmap mBitmap;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private MediaRecorder mMediaRecorder;
    private final String TAG = "CameraLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        this.mBtnCapture = findViewById(R.id.button_capture);
        this.mBtnCapture.setOnClickListener(this);

        this.mCamera = getCameraInstance();

        if (checkCameraHardware(this)) {
            if (mCamera != null) {
                this.mCameraPreview = new CameraPreview(this, mCamera);

                FrameLayout cameraPreview = findViewById(R.id.camera_preview);
                cameraPreview.addView(mCameraPreview);
            }
        }

    }

    private boolean checkCameraHardware(@NonNull Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            // Dispositivo não tem câmera
            return false;
        }
    }

    public static Camera getCameraInstance() {
        Camera camera = null;

        try {
            camera = Camera.open();
        } catch (Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
        }

        return camera;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_capture) {
            if (this.mCamera != null) {

                this.mCamera.takePicture(
                        null,
                        null,
                        new CapturePicture(this)
                );

            }
        }
    }

    @Override
    public void byteData(byte[] data, Camera camera) {

    }

    private boolean prepareVideoInstance() {

        this.mCamera = getCameraInstance();
        if (mCamera != null) {

            this.mMediaRecorder = new MediaRecorder();

            this.mCamera.unlock();
            this.mMediaRecorder.setCamera(mCamera);

            this.mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

            this.mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

            this.mMediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO));

            this.mMediaRecorder.setPreviewDisplay(mCameraPreview.getHolder().getSurface());


            try {
                this.mMediaRecorder.prepare();
            } catch (IllegalStateException e) {
                Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
                releaseMediaRecorder();
                return false;
            } catch (IOException e) {
                Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
                releaseMediaRecorder();
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    private void releaseMediaRecorder() {

    }

    private String getOutputMediaFile(int mediaTypeVideo) {
        return null;
    }


}
