package com.devmobile.tensorflow.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;

import com.devmobile.tensorflow.R;
import com.devmobile.tensorflow.task.AsyncConvertBase64;
import com.devmobile.tensorflow.task.EncodeDecode;

public class MainActivity extends AppCompatActivity implements AsyncConvertBase64.Base64Result {

    private static int REQUEST_IMAGE_CAPTURE = 1;
    private AppCompatImageView mImageViewCamera;
    private final int PERMISSION_CAMERA_CODE = 100;
    private String TAG = "MainPhoto";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mImageViewCamera = findViewById(R.id.imageCamera);

        requestPermissions();


    }


    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
                // Mensagem personalizada com dialog
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        PERMISSION_CAMERA_CODE);
            }

        } else {
            dispatchTakePictureIntent();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(
            int requestCode, int resultCode,
            @Nullable Intent intentData) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (intentData != null) {

                Bundle extras = intentData.getExtras();

                if (extras != null) {

                    Bitmap imageBitmap = (Bitmap) extras.get("data");
//                    this.mImageViewCamera.setImageBitmap(imageBitmap);
                    String encode = EncodeDecode.encodeImage(imageBitmap);
                    Bitmap bitmap = EncodeDecode.decodeImage(encode);
                    if (imageBitmap != null && encode != null) {
//                       resultConverterBase64(encode);
                        this.mImageViewCamera.setImageBitmap(bitmap);
                    }
                }

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_CAMERA_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                } else {
                    // Pedir permissão novamente
                }
                break;
        }

    }

    public void handlerTirarPhoto(View view) {
        dispatchTakePictureIntent();
    }

    private boolean checkCameraHardware(@NonNull Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            // Dispositivo não tem câmera
            return false;
        }
    }

    public static Camera getCameraInstance() {
        Camera camera = null;

        try {
            camera = Camera.open();
        } catch (Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
        }

        return camera;
    }

    @Override
    public void resultConverterBase64(@NonNull String imageBase64) {
        Bitmap bitmap = EncodeDecode.decodeImage(imageBase64);
        if (bitmap != null) {
            this.mImageViewCamera.setImageBitmap(bitmap);
        }
        Log.d(TAG, "resultConverterBase64: " + imageBase64);
    }

    @Override
    public void errorConvertBase64(@NonNull String error) {
        Log.e(TAG, "errorConvertBase64: " + error);
    }

}
