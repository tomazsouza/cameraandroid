package com.devmobile.tensorflow.capturepicture;

import android.hardware.Camera;
import android.support.annotation.NonNull;

public class CapturePicture implements Camera.PictureCallback {

    private ListenerPicture mListernerPicture;

    public CapturePicture(@NonNull ListenerPicture listernerPicture) {
        this.mListernerPicture = listernerPicture;
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        this.mListernerPicture.byteData(data, camera);
    }

    public interface ListenerPicture {
        void byteData(byte[] data, Camera camera);
    }

}
